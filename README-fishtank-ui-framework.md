# FFF SEARCH POC CREATION GUIDE

1. Initialize project
2. UI styling
3. Sources and extensions

## Initialize Project
`$ git clone https://USERNAME@bitbucket.org/USERNAME/fff_search_poc.git c:\your\repo\directory`

`$ cd c:\your\repo\directory`
`$ git init`
`$ npm i`
`$ npm audit fix`
`$ npm run build`

local server:
`$ npm run watch`
add new changes to local server:
`$ npm run build` in a new terminal parallel to the running watcher

view site:
open `localhost:3000` in a browser

## UI Styling
1. Get logo
2. Customize UI
3. QA

Get Logo:
* Find a logo on the client site
* Place the logo in `/pages/logo.svg`
* Ensure `index.html` is referencing the logo file name and path.

Customize UI
1. Create custom-color Sass file based off `colors-Default.scss`
2. Select colors from target site and assign to variables
3. Import custom color file to `fff_custom.scss`
4. Verify it looks good
5. Make any changes needed in the color-to-element mapping section at the top of `fff_custom.scss`
6. Add tabs and facets to page

## Sources and Extensions
the saga will eventually continue