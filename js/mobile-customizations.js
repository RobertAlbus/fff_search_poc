document.addEventListener("DOMContentLoaded", () => {

    var toggleFacet = () => {
        
        var isOpen = facetEl.className.indexOf("open") > -1;
        if(isOpen) {
            facetEl.className = facetEl.className.replace("open","");
            toggleTextEl.innerHTML = "Filters";
        }
        else {
            facetEl.className = facetEl.className + " open";
            toggleTextEl.innerHTML = "Close";
        }
    };
    
    console.log("_search-customization.js");

    var toggleEl = document.getElementsByClassName("coveo-facet-toggle")[0];
    var toggleTextEl = document.getElementsByClassName("coveo-facet-toggle-text")[0];
    var facetEl = document.getElementsByClassName("coveo-facet-column")[0];
    toggleEl.addEventListener("click", toggleFacet, true);

});
